<?php
require_once __DIR__ . '/vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use itlist\controllers\ControllerItem as ControllerItem;
use itlist\controllers\ControllerListe as ControllerListe;
use itlist\controllers\ControllerUtilisateur as ControllerUtilisateur;
use \itlist\divers\Outils as Outils;
use \itlist\models\Item as Item;
use \itlist\models\Liste as Liste;
use \itlist\models\Utilisateur as Utilisateur;
use \itlist\views\ViewListe as ViewListe;

/* A faire/peaufiner:
-lors de la réservation d'un item, prendre en compte le login si liste non publique
-restreindre l'affichage de l'état de la réservation si l'utilisateur connecté est le créateur (affichage poste date)
-cagnotte
 */

/* GESTION DES COMPTES
Position 0 de connexion on place l'id de l'utilisateur
Position 1 de connexion on place le nom de l'utilisateur
Position 2 de connexion on place ses droits (0 non connecté, 1 user, 2 admin)
$_SESSION['connexion'][1] = $_POST['nomAuthentification'];
 */
date_default_timezone_set('Europe/Paris');
$dateMin = date('Y-m-d', time());
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$ROOT_URI_GLOBAL = $app->request()->getRootUri();
$GESTION_COMPTE_URL_GLOBAL = $ROOT_URI_GLOBAL . '/gestion_compte';

Outils::headerHTML("Projet final php");
print("<h1>Projet PHP wishlist</h1>");
session_start();
/*************************************************************************************************************************************************************************************
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 **                                                                   Page pour se logguer                                                                                           **
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 *************************************************************************************************************************************************************************************/
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/ */
$app->get('/', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $tentativeAuthentificationURL = $rootUri . '/login';
    $formulaireCreationCompteURL = $rootUri . '/formulaire_creation_compte';
    $accesListePubliqueURL = $rootUri . '/publique';

    if (isset($_SESSION['connexion'][0])){
        print '<h1>Vous avez été déconnecté.</h1>';
    }
    ControllerUtilisateur::seDeconnecter();
    print '<center><div class="cadre"><h2>Page de connexion</h2>';

    print '<form name="formulaire auth" method="post" action =' . $tentativeAuthentificationURL . ' class="formid">
        Nom utilisateur :<br>
        <input type = text name = nomAuthentification value="" placeholder = "ex:henri4 "required>
        <br>
        Mot de passe :<br>
        <input type = "password" name = motDePasse value="" placeholder = "gardez le secret! "required>
        <input type="submit" value="Login">
        </form>';
    print
        <<< retour
        <br>Pas de compte?<br><a href='$formulaireCreationCompteURL'> Cliquez ici pour vous créer un compte</a><br><br>
retour;

    print
        <<< retour
<br><br>Vous pouvez consulter les listes publiques sans vous authentifier<br><a href='$accesListePubliqueURL'> Cliquez ici pour les consulter</a></div></center><br>
retour;
});

/* Création de compte */
$app->get('/formulaire_creation_compte', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $creationCompteURL = $rootUri . '/creation_compte';

    print '<center><div class="cadre"><br><h2>Page de création de compte</h2><br><br>';
    print '<form name="formulaire auth" method="post" action =' . $creationCompteURL . '>
        Nom utilisateur :<br>
        <input type = text required name = nomAuthentification value="">
        <br><br>
        Mot de passe :<br>
        <input type ="password" required name = motDePasse value="">
        <br><br>
        Confirmation du mot de passe :<br>
        <input type ="password" required name = confirmationMotDePasse value="">
        <input type="submit" value="Login">
        </form>';
    print
        <<< retour
            Exigences du mot de passe : 9 caractères avec au moins un chiffre et une majuscule, les espaces seront supprimés</center>
retour;
});

/* Création de compte */
$app->post('/creation_compte', function () {
    print '<center><h4>Tentative de création de compte ...</h4></center><br><br>';
    ControllerUtilisateur::creationUtilisateur();
});

/* Page de vérifiction de login */
$app->post('/login', function () {
    print 'Tentative authentification<br>';
    // Session place 0 = nom
    $_SESSION['connexion'][0] = Outils::nettoyerINPUT($_POST['nomAuthentification']);
    // Variable contenant le nom
    $pseudo = Outils::nettoyerINPUT($_POST['nomAuthentification']);
    // Variable contenant le motDePasse en clair
    $pw = Outils::nettoyerINPUT($_POST['motDePasse']);
    // Hashage du  pw
    $motDePasseHashed = password_hash($pw, PASSWORD_DEFAULT);
    // Au cas ou, vérifiaction du dur et hash
    $boolCheck = password_verify($pw, $motDePasseHashed);
    ControllerUtilisateur::authentification($pseudo, $motDePasseHashed);
});

/* Page de changer de mot de passe */
$app->get('/form_change_password', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $goUpdatePassword = $rootUri . '/DBaction_update_password';
    print '<center>Vous souhaitez modifier votre mot de passe ?<br><br>';
    print '<form name="formulaire" method="post" action =' . $goUpdatePassword . '>
        Nouveau mot de passe :<br>
        <input type ="password" required name = nouveauMotDePasse value="">
        <br><br>
        Confirmation du nouveau mot de passe :<br>
        <input type ="password" required name = new_confirmed_mdp value="">
        <input type="submit" value="Changer mot de passe">
        </form></center>';

});

$app->post('/DBaction_update_password', function () {
    ControllerUtilisateur::changerMotdepasse();
});

/* Page de gestion de compte */
$app->get('/gestion_liste', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $goModifierListe = $rootUri . '/modifierListe/';
    $goPartageListe = $rootUri . '/liste/';
    $supprimerListe = $rootUri . '/suppression_liste';


    if (!isset($_SESSION['connexion'][0])) {
        print '<br>Vous ne pouvez pas accéder à cette page tant que vous ne vous êtes pas authentifié';
        print '<br>Redirection à la page de login dans 5 secondes</h1>';
        header("refresh: 5; url=$rootUri");
    } else {
        print '<h2>Page de gestion de liste(s) associée(s) à votre compte (user_id : '.$_SESSION["connexion"][0].')</h2><br>';
        $listsFromUser = Liste::select()->where('user_id', '=', $_SESSION['connexion'][0])->get();
        if ((sizeof($listsFromUser) > 0)) {
            foreach ($listsFromUser as $value) {
                // print "<center><div class='box'><center><div class='titrebox'>$value->nom</div></center>";

                print "<center><div class='box'><h3>Titre : $value->titre</h3>";
                print "Description : $value->description<br>";
                print "Date d'expiration : $value->expiration<br>";
                print "Token de modification : $value->token_modification<br>";
                print 'Lien direct vers la  <a href=' . $goModifierListe . $value->token_modification . '>modification</a><br>';
                print 'Lien direct vers le  <a href=' . $goPartageListe . $value->token_partage . '>partage</a><br>';
                print 'Lien à envoyer à vos amis pour le partage : ' . $goPartageListe . $value->token_partage . '<br>';
                print "Token de partage seul : $value->token_partage<br>";
                print '<form method="post" action =' . $supprimerListe . '>';
                print "<input type='hidden' name=noListe value=$value->no />";
                print '<input type = submit value = "Supprimer" class="detailboutonsupp ">
                    </form></div></center><br>';
            }
        } else {
            print 'Vous n\'avez pas encore créé de liste.';
        }
        print '<br><br>';
    }
});

/* Page pour changer son mot de passe ou supprimer son compte */
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/gestion_compte */
$app->get('/gestion_compte', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $changePW = $rootUri . '/form_change_password';
    $supprimerCompte = $rootUri . '/suppression_compte';
    global $ROOT_URI_GLOBAL;
    if (isset($_SESSION['connexion'][0])) {
        print '<center><h2>Gestion de votre compte</h2>';
        print "<br><br>Vous souhaitez changer de mot de passe? <a href='$changePW'>C'est par ici</a>";
        print "<br><br>Vous avez la possibilité de supprimer votre compte, toutes vos listes et items associés seront définitivement supprimés, soyez sûr de votre coup. <a href='$supprimerCompte'>C'est par ici</a></center>";
    } else {
        print "Vous ne pouvez pas accéder à cette page sans être authentifié";
        print '<br>Redirection à la page de connexion dans 4 secondes</h1>';
        header("refresh: 4; url=$ROOT_URI_GLOBAL");
    }
});

/* Page pour supprimer son compte */
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/suppression_compte */
$app->get('/suppression_compte', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    global $ROOT_URI_GLOBAL;
    if (isset($_SESSION['connexion'][0])) {
        print '<h2>Suppression de votre compte</h2>';
        $listeDeListe = Liste::where('user_id', $_SESSION['connexion'][0])->get();
        $allItem = Item::select()->get();
        foreach ($listeDeListe as $valueLIST) {
            foreach ($allItem as $valueITEM) {
                if ($valueITEM->liste_id == $valueLIST->no) {
                    $temp = $valueITEM->nom;
                    if ($valueITEM->delete()) {
                        print "Item $temp supprimé <br>";
                    }
                }
            }
        }
        if ($deletingAllListFromUser = Liste::where('user_id', $_SESSION['connexion'][0])->delete()) {
            print "Toutes les listes de ce compte ont été supprimées<br>";
        }
        if ($deletingUser = Utilisateur::find(($_SESSION['connexion'][0]))->delete()) {
            print "<br>Votre compte a bien été supprimé";
            print '<br>Redirection à la page de connexion dans 4 secondes';
            header("refresh: 4; url=$ROOT_URI_GLOBAL");
        }
    } else {
        print "Vous ne pouvez pas accéder à cette page sans être authentifié";
        print '<br>Redirection à la page de connexion dans 4 secondes';
        header("refresh: 4; url=$ROOT_URI_GLOBAL");
    }});

    /* Page pour supprimer une liste et ses items */
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/suppression_liste */
$app->post('/suppression_liste', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $no = $_POST["noListe"];
    global $ROOT_URI_GLOBAL;
    if (isset($_SESSION['connexion'][0])) {
        print '<h2>Suppression de la liste et items associés</h2>';
        $listeAsupprimer = Liste::find($no);
        $allItem = Item::select()->get();
        
            foreach ($allItem as $valueITEM) {
                if ($valueITEM->liste_id == $listeAsupprimer->no) {
                    $temp = $valueITEM->nom;
                    if ($valueITEM->delete()) {
                        print "Item $temp supprimé <br>";
                    }
                }
            }
        
        if ($listeAsupprimer->delete()) {
            print "<h2>Votre liste est bien supprimée</h2><br>";

        } else {
            print "<h2>Problème lors de la suppression</h2>";
        }
        print '<br>Redirection à la page de vos listes dans 4 secondes';
        header("refresh: 4; url=$ROOT_URI_GLOBAL/gestion_liste");

        
    } else {
        print "Vous ne pouvez pas accéder à cette page sans être authentifié";
        print '<br>Redirection à la page de connexion dans 4 secondes';
        header("refresh: 4; url=$ROOT_URI_GLOBAL");
    }});

/*************************************************************************************************************************************************************************************
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 **                                                    Affichage des listes publiques                                                                                                **
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 *************************************************************************************************************************************************************************************/
/* Page pour les invités (affichage de listes publiques) */
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/publique */
$app->get('/publique', function () {
    global $dateMin;
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $listeURL = $rootUri . '/liste';
    print '<center><h3>Page pour les invités (affichage de listes publiques)</h3></center><br><br>';
    ViewListe :: vueListesPubliques($dateMin, $listeURL);
});

/*************************************************************************************************************************************************************************************
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 **                                Affichage d'une liste pour laisser un message dessus, consulter les réservations, et effectuer des réservations                                   **
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 *************************************************************************************************************************************************************************************/

/* Lien vers la liste 3 */
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/liste/nosecure3_partage */
$app->get('/liste/:token', function ($token) {
    ControllerListe::impressionListePartage($token);
});

/* Slim pour afficher le détail d'un item, l'url provient de /liste/nosecure1  */
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/itemDetails */
$app->post('/itemDetails', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $dbActionReservationURL = $rootUri . '/DBaction_update_reservation_item';
/* On récupère les valeurs envoyées par l'url qui sont le nom et l'id de l'item */
    $nomPushed = Outils::nettoyerINPUT($_POST["u0"]);
    $idPushed = Outils::nettoyerINPUT($_POST["u1"]);
    $listeIdPushed = Outils::nettoyerINPUT($_POST["u2"]);
    $listeExpiration = Outils::nettoyerINPUT($_POST["u3"]);
    ControllerItem::printItemDetailPlusReservation($dbActionReservationURL, $nomPushed, $idPushed, $listeIdPushed, $listeExpiration);
});

/*************************************************************************************************************************************************************************************
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 **                                                                   Création et insertion de liste                                                                                      **
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 *************************************************************************************************************************************************************************************/
/* Page pour créer une liste */
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/creerListe */
$app->get('/creerListe', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $listeFaiteURL = $rootUri . '/listeFaiteURL';
    global $dateMin;
    print "<center><h2>Section création de liste<br></h2>";
    print "Vous souhaitez créer une liste? Alors remplissez ce formulaire<br><br>";
    print '<form name="formulaire_creation_liste" method="post"  action =' . $listeFaiteURL . '>
    Titre de la liste:<br>
    <input type = text name = titre value="">
    <br>Description <br>
    <input type = text name = description value=""><br>
    <br>Expire le :<br>
    <input type = date name = expiration min=' . $dateMin . ' value=""><br>
    <input type="checkbox" id="public" name="public">
    <label for="publi">Publique</label><br>
    <input type = submit value = go>
    </form><center>';
});

/* Page pour insérer une liste */
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/listeFaiteURL */
$app->post('/listeFaiteURL', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $urlAccueil = $rootUri . '/gestion_liste';

    $test = false;
    $titre = Outils::nettoyerINPUT($_POST['titre']);
    $descr = Outils::nettoyerINPUT($_POST['description']);
    $exp = Outils::nettoyerINPUT($_POST['expiration']);

    if (isset($_POST['public'])) {
        $disponibilité = 1;
    } else {
        $disponibilité = 0;
    }
    /* On regarde si une liste comprte déjà le même nom */
    $allLists = Liste::get();
    foreach ($allLists as $value) {
        if ($value->titre == $titre) {
            $test = true;
        }
    }
    if ($test) {
        print "Il existe déjà une liste portant ce nom.<br>";
    } else {
        /* Génération d'un token aléatoire */
        $newTokenModification = ControllerListe::creer_token(20);
        $newTokenPartage = ControllerListe::creer_token(20);
        while ($newTokenModification == $newTokenPartage) {
            $newTokenPartage = ControllerListe::creer_token(20);
        }
        $insertList = new Liste();
        $insertList->titre = $titre;
        $insertList->description = $descr;
        $insertList->expiration = $exp;
        $insertList->token_modification = $newTokenModification;
        $insertList->token_partage = $newTokenPartage;
        $insertList->estPublique = $disponibilité;

        if (isset($_SESSION['connexion'][0])) {
            $insertList->user_id = $_SESSION['connexion'][0];
            print 'Liste ajoutée à votre compte<br>';} else {
            $insertList->user_id = 1;
            print '<h2>Liste ajoutée sans association à un compte, seul l\'administrateur peut retrouver vos tokens, ne perdez pas les liens de modification et partage<br></h2>';}
        if ($insertList->save()) {
            print "Insertion réussie, votre liste | $titre | a bien été créé<br><br>";
            print "Voici le lien de partage de votre liste :<br>";
            $partage = $rootUri . "/liste/$newTokenPartage";
            print $partage;
            print "<br><br>Voici le lien de modification de votre liste :<br>";
            $modification = $rootUri . "/modifierListe/$newTokenModification";
            print $modification;
        }
        if (isset($_SESSION['connexion'][0])) {
            print '<br><br>Retournez à votre page de gestion de vos listes : ';
            print "<a href='$urlAccueil'>ici</a>";
        }
    }
});

/*************************************************************************************************************************************************************************************
 **                                                                                                                                                                                  **
 **                                                Modification de liste                                                                                                             **
 **                                                création d'item associé                                                                                                           **
 **                                                envoie vers la modification d'un item associé                                                                                     **
 **                                                                                                                                                                                  **
 *************************************************************************************************************************************************************************************/
/* Modification de liste, création d'item associé, envoie vers la modification d'un item associé */
/* http://localhost/php_workspace/ProjetFinal/projetfinalphp/modifierListe/nosecure3_modification */
$app->get('/modifierListe/:monToken', function ($monToken) {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $modifierItemURL = $rootUri . '/modification_item';
    $supprimerItemURL = $rootUri . '/suppression_item';
    $modifierListeConfirmationURL = $rootUri . '/modifier_liste_confirmation';
    $creationItemURL = $rootUri . '/creation_item';
    global $dateMin;
    $sanitizedToken = Outils::nettoyerINPUT($monToken);
    /* Démarrage de session pour sauver le token de la liste demandée */
    $listeCible = Liste::select()->where("token_modification", "=", $sanitizedToken)->first();
    if (isset($listeCible)) {
        $_SESSION["ourSession"] = [$sanitizedToken, $listeCible->titre, $listeCible->no];
        /* On récupère la liste associée au token de l'url */
        $listeQueried = Liste::select()->where('token_modification', '=', $sanitizedToken)->get();
        /* On parcourt la liste pour afficher chacun de ses éléments */
        foreach ($listeQueried as $value) {
            print "<h2>Titre : $value->titre</h2>";
            print "<h3>Description : $value->description</h3>";
            print "Expiration : $value->expiration<br>";
            print "Token de modification : $value->token_modification<br>";
            print "Token de partage: $value->token_partage<br>";
            if ($value->estPublique) {
                print "Visibilité de la liste : publique<br>";
            } else {
                print "Visibilité de la liste : privée<br>";
            }
        }
        /* Récupération via la fonction association de tous les items attachés à la liste traitée */
        $itemAssociate = Liste::select()->where('token_modification', '=', $sanitizedToken)->first()->itemsFromList()->get();
        print "<br>Voici les items actuellement associés, vous pouvez les modifier en cliquant sur leur bouton associé<br><br>";
        foreach ($itemAssociate as $value) {
            /* On affiche le nom de l'item, au bout il y a un bouton qui dirige vers le Slim de modification */
            print "<center><div class='box'><center><div class='titrebox'>$value->nom</div></center>";
            
            /* On affiche son image associée */
            $quatrePremieresLettres = substr($value->img, 0, 4);
            if ($quatrePremieresLettres == 'http') {
                print "<img src = $value->img class='image'>" . "<br>";
            } else {
                print "<img src =../img/{$value->img} class='image'>" . "<br>";
            }
            /* On regarde si l'item est déjà réservé */
            if (isset($value->reservation_nom)) {
                print "$value->reservation_nom a déjà réservé cet item <br><br>";
            } else {
                print "Non réservé";
            }
            print '<form method="post" action =' . $supprimerItemURL . '>';
            print "<input type='hidden' name=id value=$value->id />";
            print '<input type = submit value = Supprimer>
                </form>';
                //Transmission de l'id de l'item sous le nom idItem via un bouton
            print '<form action=' . $modifierItemURL . ' method="post">
                <button type="Modifier" name="idItem" value=' . $value->id . ' class="btn-modif">Modifier</button>
                </form>
                </div></center><br>';
            
            
        }
        
        
        
        print "<center><br><h2>Section modification de liste<br></h2>";
        print "Remplissez les champs que vous souhaitez mettre à jour, si vous laissez un champ vide, alors ce champ ne sera pas mis à jour.";
        print '<form method="post" action =' . $modifierListeConfirmationURL . '>
        Titre de la liste :<br>
        <input type = text name = titre placeholder="Titre de la liste">
        <br>Sa description :<br>
        <input type = text name = description placeholder="Sa description">
        <br>Elle expirera le :
        <input type = date name = expiration min=' . $dateMin . ' value="">
        <br><input type="checkbox" id="public" name="public">
        <label for="public">Cochez la case si vous voulez qu\'elle soit publique</label>
        <br><input type = submit value ="Mettre à jour la liste" >
        <input type = hidden name = no value = ' . $listeCible->no . '>
        </form>';

        print "<br><br><h2>Section ajout d'item à la liste<br></h2>";
        print "Vous pouvez ajouter un item à cette liste, pour cela vous devez saisir les données relatives à cet item";
        print '<form method = "post" action =' . $creationItemURL . '>
            Nom d\'item :<br>
            <input type = text name = nom value="" required>
            <br>Description :<br>
            <input type = text name = description value="" required>
            <br>URL de l\'objet:<br>
            <input type = text name = urlObjet value="" required>
            <br>URL de l\'image : <br>
            <input type = text name = urlImage value="" required>
            <br>Prix :<br>
            <input type = float name = prix value="" required>
            <br><input type = submit value = Créer>
            </form></center><br><br><br>';
    } else {
        print "<center>Aucune liste n'existe avec le token spécifié</center>";
    }
});

// INSERTION ITEM
$app->post('/creation_item', function () {
    $nomItem = Outils::nettoyerINPUT($_POST["nom"]);
    $descrItem = Outils::nettoyerINPUT($_POST["description"]);
    $prixItem = Outils::nettoyerINPUT($_POST["prix"]);
    $urlItem = Outils::nettoyerINPUT($_POST["urlObjet"]);
    $urlImage = Outils::nettoyerINPUT($_POST["urlImage"]);
    /* Création de l'item */
    $insertItem = new Item();
    $insertItem->nom = $nomItem;
    $insertItem->descr = $descrItem;
    $insertItem->tarif = $prixItem;
    $insertItem->url = $urlItem;
    $insertItem->img = $urlImage;
    $insertItem->liste_id = Outils::nettoyerINPUT($_SESSION["ourSession"][2]);
    /* Sauvegarde de l'item dans la base de données */
    if ($insertItem->save()) {
        print "Item rattaché à la liste ";
    } else {
        print 'Erreur lors de la création de l\'item';
    }
    print '<br>Redirection à la section de modification de liste dans 5 secondes';
    header('refresh: 5;' . $_SERVER['HTTP_REFERER']);
});

/* Page de formulaire en vue de modifier un item*/
$app->post('/modification_item', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
    $dbActionUpdateItemURL = $rootUri . '/DBaction_update_item';

    $idAsked = Outils::nettoyerINPUT($_POST['idItem']);
    print "<center><div class='box'><div class='titrebox'>Vous demandez à modifier l'item ayant pour id $idAsked<br>";
    $itemAmettreAjour = Item::select()->where('id', '=', $idAsked)->first();
    print "Numéro id de l'item selectionné : " . $itemAmettreAjour->id . "<br>";
    print "Nom de l'item selectionné : " . $itemAmettreAjour->nom . "</div><br>";
    print "Description de l'item selectionné : " . $itemAmettreAjour->descr . "<br>";
    /* On affiche son image associée */
    $quatrePremieresLettres = substr($itemAmettreAjour->img, 0, 4);
    if ($quatrePremieresLettres == 'http') {
        print "<img src = $itemAmettreAjour->img class='image'>" . "<br>";
    } else {
        print "<img src = img/{$itemAmettreAjour->img} class='image'>" . "<br>";
    }
    print "<a href='$itemAmettreAjour->url' class='linkref'>url de l'item selectionné</a><br>";
    print "Prix de l'item selectionné : " . $itemAmettreAjour->tarif . "€<br>";
    print "Numéro de la liste associée : " . $itemAmettreAjour->liste_id . "<br>";

    print "Image de l'item associée : " . $itemAmettreAjour->img . "</div><center><br>";

    print "<center><br>Vous pouvez modifier chacun de ses attributs (hors id et liste_id) en remplissant les champs ci-dessous, si vous laissez un champ vide alors l'attribut associé ne sera pas modifié.<br>";
    print "<form method = post action = $dbActionUpdateItemURL>";
    print "<br>Nouveau nom : <br>";
    print '<input type =text name = newNom value ="">';
    print "<br>";
    print "Nouvelle description : <br>";
    print "<input type = text name = newDescr>";
    print "<br>";
    print "Nouvelle image : <br>";
    print "<input type = text name = newImg>";
    print "<br>";
    print "Nouvelle url : <br>";
    print "<input type = text name = newUrl>";
    print "<br>";
    print "Nouveau tarif : <br>";
    print "<input type = text name = newTarif>";
    print "<br>";
    print '<input type="submit" value="Mettre à jour">';
    print '<input type="hidden" name=id value=' . $idAsked . ' />';
    print '</form><br></center>';
});

/* Page pour mettre à jour un item dans la DB */
$app->post('/DBaction_update_item', function () {
    global $ROOT_URI_GLOBAL;
    $idAsked = Outils::nettoyerINPUT($_POST['id']);
    $newNom = Outils::nettoyerINPUT($_POST['newNom']);
    $newDescr = Outils::nettoyerINPUT($_POST['newDescr']);
    $newImg = Outils::nettoyerINPUT($_POST['newImg']);
    $newUrl = Outils::nettoyerINPUT($_POST['newUrl']);
    $newTarif = Outils::nettoyerINPUT($_POST['newTarif']);
    $isTarifAnumber = false;
    if (is_numeric($newTarif)) {
        $isTarifAnumber = true;
    }
    /* Récupération de l'item ciblé par la fonction find(primaryKey) */
    $updateItem = Item::find($idAsked);
    /* On refuse toute mise à jour d'un item déjà réservé */
    if (strlen($updateItem->reservation_nom) == 0) {
        /* Les if servent à soir si la mise à jour a été demandé (sans la vérification, tout est écrasé par des champs vides) */
        if ($newNom) {
            print "Vous avez demandé de changer le nom actuel | $updateItem->nom | en | $newNom |<br>";
            $updateItem->nom = $newNom;
        }
        if ($newDescr) {
            print "Vous avez demandé de changer la description actuelle | $updateItem->descr | en | $newDescr |<br>";
            $updateItem->descr = $newDescr;
        }
        if ($newTarif && $isTarifAnumber) {
            print "Vous avez demandé de changer le tarif actuel | $updateItem->tarif | en | $newTarif |<br>";
            $updateItem->tarif = $newTarif;
        } else if ($isTarifAnumber == false && strlen($newTarif) > 0) {
            print "Mise à jour du tarif impossible, vous n'avez pas saisi un nombre<br>";
        }
        if ($newUrl) {
            print "Vous avez demandé de changer l'url actuelle | $updateItem->url | en | $newUrl |<br>";
            $updateItem->url = $newUrl;
        }
        if ($newImg) {
            print "Vous avez demandé de changer le nom de l'image | $updateItem->img | en | $newImg |<br>";
            $updateItem->img = $newImg;
        }
        /* Sauvegarde de l'item dans la base de données */
        if ($updateItem->save()) {
            print "Votre item a bien été mis à jour.";

        } else {
            print "Erreur lors de la mise à jour";
        }
    } else {
        print "Vous ne pouvez pas modifier un item déjà réservé.";
    }
    header("refresh: 4; url=$ROOT_URI_GLOBAL/gestion_liste");
});

/* Page pour suppression d'une item dans la DB */
$app->post('/suppression_item', function () {
    /* On récupères les informations de réservation pushées par l'url */
    $idAsuppr = $_POST["id"];
    $supprItem = Item::find($idAsuppr);
    //Si l'item existe
    if ($supprItem) {
        $supprItem->delete();
        print "Item correctement supprimé";
        print '<br>Redirection à la section de modification de liste dans 5 secondes';
        header('refresh: 5;' . $_SERVER['HTTP_REFERER']);} else {
        print "Erreur lors de la suppression, l'item n'a pas été trouvé";
        print '<br>Redirection à la section de modification de liste dans 5 secondes';
        header('refresh: 5;' . $_SERVER['HTTP_REFERER']);
    }
});

/* Slim pour faire un update de liste dans la DB */
$app->post('/modifier_liste_confirmation', function () {
    $updatingList = Liste::find($_POST['no']);
    if ($_POST['titre']) {
        $newTitre = Outils::nettoyerINPUT($_POST['titre']);
        print "Vous avez demandé la mise à jour du titre en $newTitre<br>";
        $updatingList->titre = $newTitre;
    }
    if ($_POST['description']) {
        $newDescr = Outils::nettoyerINPUT($_POST['description']);
        print "Vous avez demandé la mise à jour de la description en $newDescr<br>";
        $updatingList->description = $newDescr;
    }
    if ($_POST['expiration']) {
        $newExp = Outils::nettoyerINPUT($_POST['expiration']);
        print "Vous avez demandé la mise à jour de la date en $newExp<br>";
        $updatingList->expiration = $newExp;
    }
    if (isset($_POST['public'])) {
        $updatingList->estPublique = 1;
        print "Vous avez demandé la mise à jour de type de disponibilité en public<br>";

    }
    if (!isset($_POST['public'])) {
        $updatingList->estPublique = 0;
        print "Vous avez demandé la mise à jour de type de disponibilité en privée<br>";

    }
    if ($updatingList->save()) {
        print '<h1>Mise à jour réussie</h1>';
        if ($_POST['titre']) {
            print "Mise à jour du titre.<br>";
        }
        if ($_POST['description']) {
            print "Mise à jour de la description.<br>";
        }
        if ($_POST['expiration']) {
            print "Mise à jour de la date d'échéance.<br>";
        }
        print "Mise à jour du type de disponibilité.<br>";
    } else {
        print 'Erreur lors de la mise à jour';
    }
    global $ROOT_URI_GLOBAL;
    print '<br>Redirection à la page de gestion de liste dans 8 secondes</h1>';
    header("refresh: 8; url=$ROOT_URI_GLOBAL/gestion_liste");
});

/*************************************************************************************************************************************************************************************
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 **                                                                   Réservation d'un item : update nom_reservation et message_reservation                                          **
 **                                                                                                                                                                                  **
 **                                                                                                                                                                                  **
 *************************************************************************************************************************************************************************************/
/* Mise à jour des attributs reservation_nom et reservation_message d'un item */
$app->post('/DBaction_update_reservation_item', function () {
    $app = \Slim\Slim::getInstance();
    $rootUri = $app->request()->getRootUri();
/* On récupères les informations de réservation pushées par l'url */
    $nom = Outils::nettoyerINPUT($_POST["nom"]);
    $message = Outils::nettoyerINPUT($_POST["message"]);
    $id = Outils::nettoyerINPUT($_POST["id"]);
    $listeID = Outils::nettoyerINPUT($_POST["idListe"]);
    $tokenPartage= Liste :: select('token_partage')->where('no','=', $listeID)->first();
    ControllerItem::updateItem($id, $nom, $message);
    header("refresh: 5; url=$rootUri/liste/$tokenPartage->token_partage");
    print '<br>Redirection à la page de la liste';    
});

/* Mise à jour de l'attribut commentaires d'une liste */
$app->post('/DBaction_update_liste_commentaires', function () {
    $app = \Slim\Slim::getInstance();
    global $GESTION_COMPTE_URL_GLOBAL;
    $nom = Outils::nettoyerINPUT($_POST["nom"]);
    $message = Outils::nettoyerINPUT($_POST["message"]);
    $noListe = $_POST["no"];
    ControllerListe::updateMessageListe($noListe, $nom, $message);
});

$app->get('/about', function () {
    print 
    <<< retour
    <center><h2><br<br><br>Site programmé par:<br><br>CRINON Nicolas | WERNER Nicolas | HOF Louis<br><br>
    Projet PHP de fin d'année<br><br>
    DUT Informatique <br>Année spéciale 2018/2019<br>
    IUT Charlemagne Nancy 2 </h2></center>
retour;
});

/* En placant le run à la fin, tout ce qui est au dessus s'exécute sauf les slim non demandés */
$app->run();

Outils::footerHTML();
