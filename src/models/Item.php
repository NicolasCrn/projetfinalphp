<?php 
namespace itlist\models;
class Item extends \Illuminate\Database\Eloquent\Model{
    
    protected $table = 'item';
    protected $primaryKey = 'id' ;
    public $timestamps = false ;

    public function liste() {
        return $this->belongsTo('\itlist\models\Liste', 'liste_id') ;
    }   
}
?>