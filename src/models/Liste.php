<?php 
namespace itlist\models;
class Liste extends \Illuminate\Database\Eloquent\Model{
        
    protected $table = 'liste';
    protected $primaryKey = 'no' ;
    public $timestamps = false ;

    public function itemsFromList() {
        return $this->hasMany('\itlist\models\Item', 'liste_id') ;
    }   
}
?>