<?php
namespace itlist\views;
use Illuminate\Database\Capsule\Manager as DB;
use \itlist\models\Liste as Liste;
use \itlist\controllers\ControllerListe as ControllerListe;
use \itlist\divers\Outils as Outils;


$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();

class ViewListe
{
            /* Fonction d'impression du détail d'une liste */
    public static function printListDetails($maListe)
    {
            print "Numéro de la liste : $maListe->no<br>";
            print "User_id : $maListe->user_id<br>";
            print "Titre : $maListe->titre<br>";
            print "Description : $maListe->description<br>";
            print "Expiration : $maListe->expiration<br>";
            print "Token de modification: $maListe->token_modification<br>";
            print "Token de partage: $maListe->token_partage<br>";
    }

    public static function vueListesPubliques($dateMin, $listeURL)
    {
        $liste1 = Liste::select()->where('estPublique', '=', 1)->get();
        $vide = 0;
        foreach ($liste1 as $value) {
            if (!($dateMin > $value->expiration)) {
                $vide = 1;
                print "<center><a href='$listeURL/$value->token_partage' class='linkref'>$value->titre</a><br></center>";
            }
        }
            if ($vide == 0) {
                echo '<center>Aucune liste à afficher</center>';
            }   
    }
}
?>