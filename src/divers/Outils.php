<?php

namespace itlist\divers;

class Outils {   
    
   public static function headerHTML($titre){
      $app = new \Slim\Slim();
      $app = \Slim\Slim::getInstance();
      $rootUri = $app->request()->getRootUri();
                print("
             <!DOCTYPE html>
             <html>
             <head>
             <link rel='stylesheet' type='text/css' href='$rootUri/style.css'>
             <link href='https://fonts.googleapis.com/css?family=Libre+Franklin' rel='stylesheet'>
             <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
                <title>
             $titre
                </title>
             </head>
             <body>
             <div class='bar'>
                 <ul>
                 <li><a href=$rootUri>Page d'authentification</a></li>
                 <li><a href=$rootUri/publique>Listes publiques</a></li>
                 <li><a href=$rootUri/gestion_liste>Vos listes</a></li>
                 <li><a href=$rootUri/creerListe>Créer une liste</a></li>
                 <li><a href=$rootUri/gestion_compte>Mon compte</a></li>
                 <li><a href=$rootUri/about>A propos</a></li>
                 <li><a href=$rootUri>Déconnexion</a></li>
               </ul> 
            </div>
                    ");
    }
    
    public static function footerHTML(){
                print("
             </body>
             </html>
                    ");
    } 

    public static function nettoyerINPUT($string){
      return filter_var ($string, FILTER_SANITIZE_STRING);
    }
}
?> 