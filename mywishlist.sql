SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `liste_id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `descr` text,
  `img` text,
  `url` text,
  `reservation_nom` text,
  `reservation_message` text,
  `tarif` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `item` (`id`, `liste_id`, `nom`, `descr`, `img`, `url`, `tarif`) VALUES
(1,	2,	'Champagne',	'Bouteille de champagne + flutes + jeux à gratter',	'champagne.jpg',	'https://www.nicolas-feuillatte.com',	20.00),
(2,	2,	'Musique',	'Partitions de piano à 4 mains',	'musique.jpg',	'https://itunes.apple.com/fr/genre/musique',	25.00),
(3,	2,	'Exposition',	'Visite guidée de l’exposition ‘REGARDER’ à la galerie Poirel',	'poirelregarder.jpg',	'http://www.poirel.nancy.fr/gestion_liste/',	14.00),
(4,	3,	'Goûter',	'Goûter au FIFNL',	'gouter.jpg',	'https://www.fifnl.com/',	20.00),
(5,	3,	'Projection',	'Projection courts-métrages au FIFNL',	'film.jpg',	'https://kinepolis.fr/cinemas/kinepolis-nancy',	10.00),
(6,	2,	'Bouquet',	'Bouquet de roses et Mots de Marion Renaud',	'rose.jpg',	'https://www.aquarelle.com/‎',	16.00),
(7,	2,	'Diner Stanislas',	'Diner à La Table du Bon Roi Stanislas (Apéritif /Entrée / Plat / Vin / Dessert / Café / Digestif)',	'bonroi.jpg',	'http://tablestan.free.fr/',	60.00),
(8,	3,	'Origami',	'Baguettes magiques en Origami en buvant un thé',	'origami.jpg',	'https://www.etsy.com/fr/market/origami',	12.00),
(9,	3,	'Livres',	'Livre bricolage avec petits-enfants + Roman',	'bricolage.jpg',	'https://www.amazon.fr/Copain-bricolage-guide-apprentis-bricoleurs/dp/2745965085/ref=sr_1_2?keywords=bricolage+enfant&qid=1557659882&s=books&sr=1-2',	24.00),
(10,	2,	'Diner  Grand Rue ',	'Diner au Grand’Ru(e) (Apéritif / Entrée / Plat / Vin / Dessert / Café)',	'grandrue.jpg',	'https://www.tripadvisor.fr/Restaurant_Review-g187162-d5964222-Reviews-LE_GRAND_RUE_ex_RESTAURANT_BAGOT-Nancy_Meurthe_et_Moselle_Grand_Est.html',	59.00),
(11,	0,	'Visite guidée',	'Visite guidée personnalisée de Saint-Epvre jusqu’à Stanislas',	'place.jpg',	'https://www.nancy-tourisme.fr/espace-pro/espace-groupes-et-organisateurs-de-voyages/visites-guidees/',	11.00),
(12,	2,	'Bijoux',	'Bijoux de manteau + Sous-verre pochette de disque + Lait après-soleil',	'bijoux.jpg',	'https://www.juliendorcel.com/bijoux.html',	29.00),
(19,	3,	'Jeu contacts',	'Jeu pour échange de contacts',	'contact.png',	'https://www.letudiant.fr/college/5e/mon-correspondant-debarque-comment-bien-l-accueillir.html',	5.00),
(22,	1,	'Concert',	'Un concert à Nancy',	'concert.jpg',	'https://www.infoconcert.com/ville/nancy-2303/concerts.html',	17.00),
(23,	1,	'Appart Hotel',	'Appart’hôtel Coeur de Ville, en plein centre-ville',	'apparthotel.jpg',	'https://www.booking.com/hotel/fr/appart-39-coeur-de-ville.fr.html',	56.00),
(24,	2,	'Hôtel d\'Haussonville',	'Hôtel d\'Haussonville, au coeur de la Vieille ville à deux pas de la place Stanislas',	'hotel_haussonville_logo.jpg',	'http://hotel-haussonville.fr/',	169.00),
(25,	1,	'Boite de nuit',	'Discothèque, Boîte tendance avec des soirées à thème & DJ invités',	'boitedenuit.jpg',	'https://www.facebook.com/ChatNoirNCY/',	32.00),
(26,	1,	'Planètes Laser',	'Laser game : Gilet électronique et pistolet laser comme matériel, vous voilà équipé.',	'laser.jpg',	'https://www.planeteslaser.com/',	15.00),
(27,	1,	'Fort Aventure',	'Découvrez Fort Aventure à Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut à l\'élastique inversé, Toboggan géant... et bien plus encore.',	'fort.jpg',	'https://www.fort-aventure.com/',	25.00);

DROP TABLE IF EXISTS `liste`;
CREATE TABLE `liste` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `expiration` date DEFAULT NULL,
  `token_modification` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `token_partage` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentaires` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `estPublique` boolean DEFAULT 0,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `liste` (`no`, `user_id`, `titre`, `description`, `expiration`, `token_modification`, `token_partage`, `estPublique` ) VALUES
(1,	1,	'Pour fêter le bac !',	'Pour un week-end à Nancy qui nous fera oublier les épreuves. ',	'2020-06-27',	'nosecure1_modification', 'nosecure1_partage', 1),
(2,	1,	'Liste de mariage d\'Alice et Bob',	'Nous souhaitons passer un week-end royal à Nancy pour notre lune de miel :)',	'2020-06-30',	'nosecure2_modification', 'nosecure2_partage', 1),
(3,	1,	'C\'est l\'anniversaire de Charlie',	'Pour lui préparer une fête dont il se souviendra :)',	'2019-12-12',	'nosecure3_modification', 'nosecure3_partage', 0);

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE `utilisateur` (
    `user_id` int(10) NOT NULL AUTO_INCREMENT,
    `user_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
    `user_pw` varchar(256) DEFAULT NULL,
    `estAdmin` boolean DEFAULT 0,
    `dates`datetime NOT NULL, 
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `user_name`(`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `utilisateur` (`user_name`, `user_pw`, `estAdmin`) VALUES
('admin', '$2y$10$gU1IS0swGsZGfZwaBIVmBe38CepWxGD3eJdlYGmXaIPz0gheF04/e', 2);